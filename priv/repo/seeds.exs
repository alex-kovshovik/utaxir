# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Utaxir.Repo.insert!(%Utaxir.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.


alias Utaxir.Repo
alias Utaxir.Books.Author
alias Utaxir.Books.Book

jose = Repo.insert!(%Author{name: "José Valim"})
chris = Repo.insert!(%Author{name: "Chris McCord"})
ulisses = Repo.insert!(%Author{name: "Ulisses Almeida"})

Repo.insert!(%Book{author_id: chris.id, title: "Programming Phoenix 1.4"})
Repo.insert!(%Book{author_id: chris.id, title: "Metaprogramming Elixir"})

Repo.insert!(%Book{author_id: jose.id, title: "Adopting Elixir: From Concept to Production"})
Repo.insert!(%Book{author_id: jose.id, title: "Crafting Rails 4 Applications: Expert Practices for Everyday Rails Development (The Facets of Ruby)"})

Repo.insert!(%Book{author_id: ulisses.id, title: "Learn Functional Programming with Elixir"})
