defmodule UtaxirWeb.PageControllerTest do
  use UtaxirWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Hello Utah Elixir!"
  end
end
