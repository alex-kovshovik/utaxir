# Sample command to run this docker image
# docker run -it -e SECRET_KEY_BASE=verysecretsecretlolhahaelonmuskteslaspacexgotomarshumans -e DB_HOST="" -e DB_NAME=utaxir_prod -e DB_USER=alexk -e DB_PASSWORD="" utaxir /bin/bash

#================================================================================
# BUILD IMAGE
#================================================================================
FROM elixir:1.8 as build

ENV HOME=/app
ENV MIX_ENV=prod REPLACE_OS_VARS=true SHELL=/bin/bash

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && \
    apt-get install -y nodejs && \
    mix local.rebar --force && \
    mix local.hex --force 

WORKDIR /app

# Cache elixir deps.
COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

# Cache node deps.
RUN mkdir assets
COPY assets/package.json assets/package-lock.json ./assets/
RUN cd assets && npm install

COPY . .

# Build and digest static assets.
RUN cd assets && npm run deploy && cd .. && mix phx.digest

# Build the release paclage!
RUN mix release --env=prod

#================================================================================
# RUN IMAGE
#================================================================================
FROM elixir:1.8 as run

ENV HOME=/app
ENV MIX_ENV=prod REPLACE_OS_VARS=true SHELL=/bin/bash

WORKDIR /app

# This is needed for troubleshooting
RUN apt-get update && apt-get install -y postgresql-client

# Use the release package from the build container.
COPY --from=build /app/_build/prod/rel/utaxir .

# ENTRYPOINT ["/app/bin/utaxir"]
CMD trap '/app/bin/utaxir stop' INT; /app/bin/utaxir foreground
