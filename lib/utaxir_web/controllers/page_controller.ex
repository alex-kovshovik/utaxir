defmodule UtaxirWeb.PageController do
  use UtaxirWeb, :controller

  alias Utaxir.Books

  def index(conn, _params) do
    render(conn, "index.html", books: Books.list_books())
  end
end
