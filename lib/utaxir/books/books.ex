defmodule Utaxir.Books do
  alias Utaxir.Repo
  alias Utaxir.Books.Book

  def list_books do
    Book
    |> Repo.all()
    |> Repo.preload(:author)
  end
end
