defmodule Utaxir.Repo do
  use Ecto.Repo,
    otp_app: :utaxir,
    adapter: Ecto.Adapters.Postgres
end
